version = 3 -- Lua Version. Dont touch this
ScenarioInfo = {
    name = "Clueless",
    description = "",
    preview = '',
    map_version = 1,
    type = 'skirmish',
    starts = true,
    size = {1024, 1024},
    reclaim = {2430, 0},
    map = '/maps/Clueless_core/Clueless.scmap',
    save = '/maps/Clueless_core/Clueless_save.lua',
    script = '/maps/Clueless_core/Clueless_script.lua',
    norushradius = 40,
    Configurations = {
        ['standard'] = {
            teams = {
                {
                    name = 'FFA',
                    armies = {'ARMY_1', 'ARMY_2'}
                },
            },
            customprops = {
                ['ExtraArmies'] = STRING( 'ARMY_17 NEUTRAL_CIVILIAN' ),
            },
        },
    },
}
